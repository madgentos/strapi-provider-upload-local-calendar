'use strict';

/**
 * Module dependencies
 */

// Public node modules.
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const {errors} = require('strapi-plugin-upload');

module.exports = {
  init({sizeLimit = 1000000} = {}) {
    const verifySize = file => {
      if (file.size > sizeLimit) {
        throw errors.entityTooLarge();
      }
    };
    const configPublicPath = strapi.config.get(
      'middleware.settings.public.path',
      strapi.config.paths.static
    );

    const uploadDir = path.resolve(strapi.dir, configPublicPath);

    return {
      upload(file) {
        verifySize(file);

        return new Promise((resolve, reject) => {
          // write file in public/assets folder
          const fileDirPath = `/uploads/${moment().format("YYYY")}/${moment().format("MMMM")}`
          const filepath = path.join(uploadDir, fileDirPath, `${file.hash}${file.ext}`)
          folderResolver(path.join(uploadDir,fileDirPath))

          fs.writeFile(filepath,
            file.buffer,
            err => {
              if (err) {
                return reject(err);
              }

              file.url = `${fileDirPath}/${file.hash}${file.ext}`;

              resolve();
            }
          );
        });
      },
      delete(file) {
        console.log(file);
        return new Promise((resolve, reject) => {
          const filePath = path.join(uploadDir, file.url);

          if (!fs.existsSync(filePath)) {
            return resolve("File doesn't exist");
          }

          // remove file from public/assets folder
          fs.unlink(filePath, err => {
            if (err) {
              return reject(err);
            }

            resolve();
          });
        });
      },
    };
  },
};

function folderResolver(dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, {recursive: true});
  }
}
